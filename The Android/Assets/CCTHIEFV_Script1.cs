﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCTHIEFV_Script1 : MonoBehaviour {

	public GameObject theThief;
	public GameObject[] theCivilian;
	GameObject theRandomCivilian1;
	GameObject theRandomCivilian2;
	GameObject theRandomCivilian3;

	public Renderer rend;

	public float countdown;
	public float countdown1;
	public float countdown2;
	public float countdown3;

	public float mincount;
	public float maxcount;

	public float Qrangexmin;
	public float Qrangexmax;
	public float Qrangeymin;
	public float Qrangeymax;


	int windowNo;
	int windowNo1;
	int windowNo2;
	int windowNo3;

	int flag1=0;
	int flag2=0;
	int flag3=0;

	int ThievesToFind;
	Text secs;
	// Use this for initialization
	void Start () {


		//countdown = Random.Range (mincount, maxcount);

		secs = GameObject.Find("Seconds").GetComponent<Text> ();

		ThievesToFind = Random.Range (3,5);

	}	
	
	// Update is called once per frame
	void Update () {
		int i = 0;
		int randomNum = 0;
		countdown -= Time.deltaTime;
		countdown1 -= Time.deltaTime;
		countdown2 -= Time.deltaTime;
		countdown3 -= Time.deltaTime;

		//secs.text = "Remaining"+countdown;


		if (countdown < 0) {
			
			countdown = Random.Range (10,15);

			windowNo = Random.Range (1, 4);
;
			Vector2 pos;


			Instantiate (theThief);

			if(windowNo == 1){
				Qrangexmin = -51;
				Qrangexmax = 191;
				Qrangeymin = 185;				
				Qrangeymax = 202;
			}
			else if(windowNo == 2){
				Qrangexmin = 264;
				Qrangexmax = 516;				
				Qrangeymin = 232;
				Qrangeymax = 232;
			}
			else if(windowNo == 3){
				Qrangexmin = -54;
				Qrangexmax = 193;
				Qrangeymin = 43;
				Qrangeymax = 69;

			}
			else if(windowNo == 4){
				Qrangexmin = 260;
				Qrangexmax = 504;
				Qrangeymin = 47;
				Qrangeymax = 59;
			}

			pos = new Vector2 (Random.Range (Qrangexmin, Qrangexmax), Random.Range (Qrangeymin, Qrangeymax));
	
			theThief.transform.position = pos;
		

			rend = theThief.GetComponent<Renderer>();

			rend.enabled = true;
		}

		if (countdown1< 0) {
			
//			if(flag1 == 1){
//				Destroy (theRandomCivilian1.gameObject);
//				flag1 = 0;
//			}
			for(i = 0; i<8; i++){
				randomNum = Random.Range (0,5); //pick item from theObjects[]
				theRandomCivilian1 = theCivilian[randomNum]; //ibigay na kay theRandomObjects

				//rend[i] = theRandomObjects[i];
			}

			countdown1 = Random.Range (6,10);

			windowNo = Random.Range (1, 4);
			Vector2 pos1;

			Instantiate (theRandomCivilian1);
			flag1 = 1;
			if(windowNo == 1){
				Qrangexmin = -51;
				Qrangexmax = 191;
				Qrangeymin = 185;				
				Qrangeymax = 202;
			}
			else if(windowNo == 2){
				Qrangexmin = 264;
				Qrangexmax = 516;				
				Qrangeymin = 232;
				Qrangeymax = 232;
			}
			else if(windowNo == 3){
				Qrangexmin = -54;
				Qrangexmax = 193;
				Qrangeymin = 43;
				Qrangeymax = 69;

			}
			else if(windowNo == 4){
				Qrangexmin = 260;
				Qrangexmax = 504;
				Qrangeymin = 47;
				Qrangeymax = 59;
			}


			pos1 = new Vector2 (Random.Range (Qrangexmin, Qrangexmax), Random.Range (Qrangeymin, Qrangeymax));

			theRandomCivilian1.transform.position = pos1;

			rend = theThief.GetComponent<Renderer>();

			rend.enabled = true;
		}

		if (countdown2 < 0) {

//			if(flag2 == 1){
//				Destroy (theRandomCivilian2.gameObject);
//				flag2 = 0;
//			}
			for(i = 0; i<8; i++){
				randomNum = Random.Range (0,5); //pick item from theObjects[]
				theRandomCivilian2 = theCivilian[randomNum]; //ibigay na kay theRandomObjects

				//rend[i] = theRandomObjects[i];
			}

			countdown2 = Random.Range (6,10);

			windowNo = Random.Range (1, 4);


			Vector2 pos2;

			Instantiate (theRandomCivilian2);
			flag2=1;
			if(windowNo == 1){
				Qrangexmin = -51;
				Qrangexmax = 191;
				Qrangeymin = 185;				
				Qrangeymax = 202;
			}
			else if(windowNo == 2){
				Qrangexmin = 264;
				Qrangexmax = 516;				
				Qrangeymin = 232;
				Qrangeymax = 232;
			}
			else if(windowNo == 3){
				Qrangexmin = -54;
				Qrangexmax = 193;
				Qrangeymin = 43;
				Qrangeymax = 69;

			}
			else if(windowNo == 4){
				Qrangexmin = 260;
				Qrangexmax = 504;
				Qrangeymin = 47;
				Qrangeymax = 59;
			}

			pos2 = new Vector2 (Random.Range (Qrangexmin, Qrangexmax), Random.Range (Qrangeymin, Qrangeymax));

			theRandomCivilian2.transform.position = pos2;
	
			rend = theThief.GetComponent<Renderer>();

			rend.enabled = true;
		}
		if (countdown3 < 0) {

//			if(flag3 == 1){
//				Destroy (theRandomCivilian3.gameObject);
//				flag3 = 0;
//			}

			for(i = 0; i<8; i++){
				randomNum = Random.Range (0,5); //pick item from theObjects[]
				theRandomCivilian3 = theCivilian[randomNum]; //ibigay na kay theRandomObjects

				//rend[i] = theRandomObjects[i];
			}

			countdown3 = Random.Range (6,10);

			windowNo = Random.Range (1, 4);

			Vector2 pos3;

			Instantiate (theRandomCivilian3);
			flag3=1;
			if(windowNo == 1){
				Qrangexmin = -51;
				Qrangexmax = 191;
				Qrangeymin = 185;				
				Qrangeymax = 202;
			}
			else if(windowNo == 2){
				Qrangexmin = 264;
				Qrangexmax = 516;				
				Qrangeymin = 232;
				Qrangeymax = 232;
			}
			else if(windowNo == 3){
				Qrangexmin = -54;
				Qrangexmax = 193;
				Qrangeymin = 43;
				Qrangeymax = 69;

			}
			else if(windowNo == 4){
				Qrangexmin = 260;
				Qrangexmax = 504;
				Qrangeymin = 47;
				Qrangeymax = 59;
			}
			pos3 = new Vector2 (Random.Range (Qrangexmin, Qrangexmax), Random.Range (Qrangeymin, Qrangeymax));

			theRandomCivilian3.transform.position = pos3;

			rend = theThief.GetComponent<Renderer>();

			rend.enabled = true;
		}

		secs.text = countdown+"";

	}
}

//	int RandomQuadrant (){
//
//
//	}
