using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.IO;

public class Diffuse_Script : MonoBehaviour {
	float countdown = 30.0f;
	Dictionary<string,string> dictionary;
	string[] strArr = new string[20];
	private Text txtCountdown;
	Text txtQuestion;
	InputField inputAnswer;
	Canvas cnvWin;
	Canvas cnvContinue;
	//Canvas cnvContinue;
	// Use this for initialization
	void Start () {
		dictionary = new Dictionary<string,string>();
		dictionary = Load();
		txtCountdown = GameObject.Find("Countdown").GetComponent<Text>();
		txtQuestion = GameObject.Find("Question").GetComponent<Text>();
		inputAnswer = GameObject.Find("Answer").GetComponent<InputField>();
		cnvWin = GameObject.Find("Canvas_Win").GetComponent<Canvas>();
		cnvContinue = GameObject.Find("Canvas_Continue").GetComponent<Canvas>();
		cnvWin.enabled = false;
		cnvContinue.enabled = false;
		System.Random random = new System.Random();
		int rand = random.Next(0,dictionary.Count-1);
		txtQuestion.text = strArr[rand];
		Debug.Log(txtQuestion);
		
	}
	bool blReturnKey = false;
	void OnGUI() {
		Event e = Event.current;
		if (e.keyCode == KeyCode.Return) {
			blReturnKey = true;
		}
	}
	bool stop = false;
	// Update is called once per frame
	
	public void submit(){
		blReturnKey = true;
	}
	
	void Update () {
		if(!stop){
			countdown -= Time.deltaTime;
		}
		//if(nputAnswer.isFocused && inputAnswer.text != "" && inputAnswer.GetKey(KeyCode.Return)) {
		if(blReturnKey){
			if(inputAnswer.text == dictionary[txtQuestion.text]&&inputAnswer.text!=null){
				txtQuestion.text = "BOMB DIFFUSED";
				CanvasWin();
				Debug.Log("win");
			}else{
				countdown -= 3;
				inputAnswer.text = "";
			}
			blReturnKey = false;
		}
		/*if(Event.current.type == EventType.keyDown && Event.current.character == '\n'){
			if(inputAnswer.text == dictionary[txtQuestion.text]&&inputAnswer.text!=null){
				txtQuestion.text = "BOMB DIFFUSED";
				Debug.Log("win");
			}
		}*/
		//}
		
		if ( countdown <= 0 )
		{
			//txtQuestion.text = "GAME OVER";
			txtCountdown.text = "0.0000";
			CanvasContinue();
		}else{
			
			txtCountdown.text = countdown+"";
		}
		
	}
	void CanvasContinue(){
		cnvContinue.enabled = true;		
	}
	
	public void Continue(){
		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold -= 200;
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);
		PlayerPrefs.Save();
		Debug.Log(fltCurrGold);
		countdown +=30;
		cnvContinue.enabled = false;
	}
	
	void CanvasWin(){
		cnvWin.enabled = true;
		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold += 50;
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);
		PlayerPrefs.Save();
		Debug.Log(fltCurrGold);
	}	
	
	public void GameOver(){
		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold -= 200;		
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);		
		PlayerPrefs.Save();
		Debug.Log("I quit");
		Application.LoadLevel("Map Scene");
	}


	private Dictionary<string,string> Load()
	{
		Dictionary<string,string>dictTemp = new Dictionary<string, string>();
		dictTemp.Add("1*36+52+65-2 = ?","151");
		dictTemp.Add("| -3 | = ?","3");
		dictTemp.Add("DCCVII","707");
		dictTemp.Add("a^2 = 9, a^4 = ?","81");
		dictTemp.Add("-20 , ? , -12 , -8, -4","-16");
		dictTemp.Add("10, 8, 16, 14, 28, 26, 52 ?","50");
		dictTemp.Add("5! = ? HINT-5*4*3*2*1","120");
		dictTemp.Add("0, 2, 6, 12, 20, 30, 42, ? HINT - 0,+2 2,+4 6,+6 12,+8 20,+10 30,+12 42", "56");
		dictTemp.Add("XXI","21");
		dictTemp.Add("CM","900");
		dictTemp.Add("55 89 144 ? Hint - use fibonacci","233");
		dictTemp.Add("610 987 1597 ? Hint - use fibonnacci","2584");
		dictTemp.Add("1 ? 3 5 Hint - use fibonacci", "2");
		dictTemp.Add("13 ? 34 55 Hint - use fibonacci","21");
		dictTemp.Add("1 2 3 ? Hint - use fibonacci","5");
		dictTemp.Add("144 233 377 ? Hint - use fibonacci","610");
		dictTemp.Add("9^4","6561");
		dictTemp.Add("3, 3, 4, 8, 10, 30, 33, ? Hint -3,×1 3,+1 4,×2 8,+2 10,×3 30,+3 33,×4","132");
		dictTemp.Add("LXXXV","85");
		dictTemp.Add("6! = ?","720");
		var keyCol = dictTemp.Keys;
		var keyList = new List<string>(keyCol);
		strArr = keyList.ToArray();
		return dictTemp;
	}
}
