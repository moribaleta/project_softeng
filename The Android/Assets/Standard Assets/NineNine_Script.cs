﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NineNine_Script : MonoBehaviour {
	
	List<QuestionBatch>list = new List<QuestionBatch>();
	InputField txtAnswer;
	int intCurrBatch = 0;
	Image[] imageSet  = new Image[8];
	Text[] txtSetAnswer = new Text[5];
	Canvas cnvWin;
	Canvas cnvHint;
	// Use this for initialization
	void Start () {
		list = Load();
		cnvWin = GameObject.Find("Canvas_Win").GetComponent<Canvas>();
		cnvWin.enabled = false;
		cnvHint = GameObject.Find("Canvas_Hint").GetComponent<Canvas>();
		cnvHint.enabled = false;
		
		txtAnswer = GameObject.Find("Text_Answer").GetComponent<InputField>();
		for(int i=0; i<8; i++){
			imageSet[i] = GameObject.Find("Image_"+i).GetComponent<Image>();
			imageSet[i].GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
		}
		
		System.Random random = new System.Random();
		intCurrBatch = random.Next(0,7);
		imageSet[intCurrBatch].GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
		Debug.Log("intCurrBatch: "+intCurrBatch);		
		//LoadCurrQuestion();
	}
	
	// Update is called once per frame
	int intMatch = 5;
	public void Submit(){
		string strAnswer = txtAnswer.text;
		for(int i = 0; i<5; i++){
			Debug.Log(list[intCurrBatch].arrStringSet[i]);
			if(
				((list[intCurrBatch].arrStringSet[i].ToUpper() == strAnswer.ToUpper())
				||
				(list[intCurrBatch].arrStringSet[i].IndexOf(strAnswer.ToUpper())!=-1&&strAnswer.Length>3))&&
				!list[intCurrBatch].alreadyInput(list[intCurrBatch].arrStringSet[i])){
				txtSetAnswer[i] = GameObject.Find("Answer"+i).GetComponent<Text>();
				txtSetAnswer[i].text = list[intCurrBatch].arrStringSet[i];
				list[intCurrBatch].answer(list[intCurrBatch].arrStringSet[i]);
				intMatch--;
				
				break;
			}
		}
		txtAnswer.text = "";
		if(intMatch<=0){
			Debug.Log("win");
			CanvasWin();
		}
	}
	
	void CanvasWin(){
		cnvWin.enabled = true;
		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold += 50;
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);
		PlayerPrefs.Save();
		Debug.Log(fltCurrGold);
	}
	
	public void openHint(bool blOpen){
		cnvHint.enabled = blOpen;
	}
	
	public void GetHint(){
		string strHint = list[intCurrBatch].getHint();
		for(int i = 0; i<5; i++){
			Debug.Log(list[intCurrBatch].arrStringSet[i]);
			if(list[intCurrBatch].arrStringSet[i].ToUpper() == strHint){
				txtSetAnswer[i] = GameObject.Find("Answer"+i).GetComponent<Text>();
				txtSetAnswer[i].text = list[intCurrBatch].arrStringSet[i];
				intMatch--;
				break;
			}
		}		
		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold -= 200;
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);
		PlayerPrefs.Save();
		Debug.Log(fltCurrGold);		
		cnvHint.enabled = false;
	}
	
	List<QuestionBatch> Load(){
		List<QuestionBatch>tempQuestion = new List<QuestionBatch>();
		string[] arr1 = {"STUDENTS","SCHOOL","BULLYING","RACIST","PHOTOGRAPHY"};				
		tempQuestion.Add(new QuestionBatch(arr1));
		string[] arr2 = {"VIOLENCE","ABUSE","TORTURE","HUSBAND","WIFE"};
		tempQuestion.Add(new QuestionBatch(arr2));
		string[] arr3 = {"TERRORIST","GUNS","ARMY","BOMBS","CAPTURE"};
		tempQuestion.Add(new QuestionBatch(arr3));
		string[] arr4 = {"DRUGS","MONEY","EXCHANGE","SCHOOL","ADDICTION"};
		tempQuestion.Add(new QuestionBatch(arr4));
		string[] arr5 = {"PICKPOCKET","WALLET","POCKET","CULPRIT","PANTS"};
		tempQuestion.Add(new QuestionBatch(arr5));
		string[] arr6 = {"PRISON","JAIL BREAKER","ESCAPE","HANDCUFFS","FIELD"};
		tempQuestion.Add(new QuestionBatch(arr6));
		string[] arr7 = {"ROBBERY","CCTV","ALARM","CAUGHT","SHOCK"};
		tempQuestion.Add(new QuestionBatch(arr7));
		string[] arr8 = {"SHOPLIFTING","BAG","CHOCOLATE","STORE","FOOD"};
		tempQuestion.Add(new QuestionBatch(arr8));
		return tempQuestion;		
		
	}
}



public class QuestionBatch{
	
	//public List<CustomDictionary> custDict = new List<CustomDictionary>();
	public string[] arrStringSet = new string[5];
	Dictionary<string,bool>dictStringSet = new Dictionary<string, bool>();
	public QuestionBatch(string[] arrString){
		foreach(string key in arrString){
			dictStringSet.Add(key,false);
		}
		this.arrStringSet = arrString;
		
		this.dictStringSet = dictStringSet;
	}
	
	public void answer(string strAnswer){		
		dictStringSet[strAnswer] = true;
	}
	
	public bool alreadyInput(string strAnswer){
		return dictStringSet[strAnswer];
	}
	
	public string getHint(){
		string strAnswer = null;
		foreach(KeyValuePair<string, bool> entry in dictStringSet)
		{
			if(entry.Value==false){
				strAnswer = entry.Key;
				break;
			}
			// do something with entry.Value or entry.Key
		}
		
		dictStringSet[strAnswer] = true;
		return strAnswer;
	}
	
}