﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomXY : MonoBehaviour {
	//private Vector3 startPosition;
	public Renderer rend;
	public GameObject theObject;
	//public GameObject[] objects;
	public Vector2[] positions;
	public float xMin;
	public float xMax;
	public float yMin;
	public float yMax;

	// Use this for initialization
	void Start () {
		rend.GetComponent<Renderer> ();
		rend.enabled = false;
		Call ();


		//startPosition = transform.position;

		//GameObject goodsPrefab = listobject [Random.Range (0, listobject.Length)];
		//GameObject myObj = Instantiate (listobject[0]) as GameObject;
		//myObj.transform.position = transform.position;
	}

	// Update is called once per frame
	void Call(){
		int i = 0;
		int j = 0;

		for(i=0;i<positions.Length;i++){
			//Instantiate (theObject);
			Vector2 pos = new Vector2 (Random.Range (xMin, xMax), Random.Range (yMin, yMax));
			positions [i] = pos;
		}

		int rand = Random.Range (1, positions.Length);
		transform.position = positions [rand];
		rend.enabled = true;
		/*Vector2 pos = new Vector2 (Random.Range (xMin, xMax), Random.Range (yMin, yMax));
		for (int i = 0; i < objects.Length; i++) {
			objects [i] = listobject [Random.Range (0, listobject.Length)];
			Instantiate (objects [i], pos, transform.rotation);

		}*/
	}

}