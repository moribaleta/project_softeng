﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Status : MonoBehaviour {
			
	public void NewUser(){
		if(PlayerPrefs.GetFloat("PlayerGold")==0&&PlayerPrefs.GetInt("NewUser") == 0){
			PlayerPrefs.SetFloat("PlayerGold",3000);
			PlayerPrefs.SetInt("NewUser",1);
			PlayerPrefs.Save();
		}
		
		
	}
	
	public void FinishedGameAddCoin(){
		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold += 50;
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);	
		Debug.Log(fltCurrGold);
		PlayerPrefs.Save();
	}
	
	public void AddCoin(float fltGold){
		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold += fltGold;
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);		
		PlayerPrefs.Save();
	}
	
	public void DeleteCoin(){
		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold -= 200;
		
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);		
		PlayerPrefs.Save();
	}
		
}
