﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map_Script : MonoBehaviour {

	// Use this for initialization
	Canvas cnvMission;
	Text txtMission;
	string strGame = null;
	Text txtGoldStatus;
	List<GameDesc>lstGame = new List<GameDesc>();
	Button[] btnGame = new Button[5];
	int intCurrBtn;
	Text txtTimer;
	float countdown = 0;
	System.Random random = new System.Random();
	
	void Start () {
		cnvMission = GameObject.Find("Canvas_Mission").GetComponent<Canvas>();
		txtMission = GameObject.Find("Text_Mission").GetComponent<Text>();
		txtGoldStatus = GameObject.Find("Text_Gold").GetComponent<Text>();
		txtTimer = GameObject.Find("Text_Timer").GetComponent<Text>();
		Player_Status ps = GameObject.FindObjectOfType(typeof(Player_Status)) as Player_Status;
		txtGoldStatus.text = PlayerPrefs.GetFloat("PlayerGold")+"";
		for(int i = 0; i<5; i++){
			btnGame[i] = GameObject.Find("Button_"+i).GetComponent<Button>();
			btnGame[i].enabled = false;
			btnGame[i].GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
		}
		GetPoints();		
		ps.NewUser();
		cnvMission.enabled = false;
		Load();
	}
	
	public void PanelOpen(int i){
		GameDesc gmdOpen = lstGame[i];
		string strDesc =  gmdOpen.strGame;
		strDesc += "\n"+ gmdOpen.strDesc;
		strDesc += "\n"+ gmdOpen.strDetailTime;
		strDesc += "\n"+ gmdOpen.strDetailOther;
		Debug.Log(strDesc);
		txtMission.text = strDesc+"";
		cnvMission.enabled = true;
		strGame = gmdOpen.strGame;
		intCurrBtn = i;
	}
	public void Decline(){
		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold -= 200;
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);
		PlayerPrefs.Save();
		txtGoldStatus.text = PlayerPrefs.GetFloat("PlayerGold")+"";
		cnvMission.enabled = false;
		btnGame[intCurrBtn].enabled = false;
		btnGame[intCurrBtn].GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
	}
	
	
	public void OpenGame(){
		SavePoints();
		btnGame[intCurrBtn].enabled = false;
		btnGame[intCurrBtn].GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
		Application.LoadLevel(strGame);
	}
	
	public void SavePoints(){
		string strOutput = "";
		for(int i = 0; i<5; i++){
			if(btnGame[i].enabled==true){
				strOutput += i+",";
			}
		}
		PlayerPrefs.SetFloat("map_time",countdown);
		PlayerPrefs.SetString("map_coordinates",strOutput);
		PlayerPrefs.Save();
	}
	
	public void GetPoints(){
		int[] arrInt = new int[5];
		string strPoints = PlayerPrefs.GetString("map_coordinates");
		if(strPoints!=null){
			string[] arrStrPoints = strPoints.Split(',');
			for(int i = 0; i<arrStrPoints.Length;i++){
				//int numVal = Int32.Parse(arrStrPoints[i]);
				int numVal;
				Int32.TryParse(arrStrPoints[i], out numVal);
				btnGame[numVal].enabled = true;
				btnGame[numVal].GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
			}
		}else{
			int rand = random.Next(1,5);
			btnGame[rand-1].enabled = true;
			btnGame[rand-1].GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
		}
		if(PlayerPrefs.HasKey("map_time")==true){
			countdown = PlayerPrefs.GetFloat("map_time");
		}
		else{
			countdown = 30;
		}
	}
	
	public void PanelClose(){
		cnvMission.enabled = false;
	}

	int getActiveMission(){
		int intCount = 0;
		for(int i = 0; i<btnGame.Length; i++){
			if(btnGame[i].enabled ==true){
				intCount++;
			}
		}
		return intCount;
	}
	
	// Update is called once per frame
	
	
	void Update () {
		countdown -= Time.deltaTime;
		if(countdown < 0){
			countdown = 30;
			if(getActiveMission()<=3){
				int rand = random.Next(1,5);
				while(btnGame[rand-1].enabled == true){
				rand = random.Next(1,5);
				}
				btnGame[rand-1].enabled = true;
				btnGame[rand-1].GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
			}
			//SavePoints();
		}
		SavePoints();
		txtTimer.text = countdown+"";
	}
	
	public void Load(){
		lstGame.Add(new GameDesc("CCthiefV","Catch the thief before the time runs out","Alloted Time: 3:00","No. of thief: "));
		lstGame.Add(new GameDesc("Drugs","Find the drugs in the given location.","alloted time: 3:00","No. of taps: \nNo. of drugs: "));
		lstGame.Add(new GameDesc("Diffuiz","Answer the question before the time runs out","Alloted Time: 1:00",""));
		lstGame.Add(new GameDesc("Jailbreaker","A prisoner escape, make sure to catch the jailbreaker.","Alloted Time: 3:00","Location of the Jailbreaker"));
		lstGame.Add(new GameDesc("99.99%","Enter the words that are related to the picture given .","",""));
	}
}

public class GameDesc{
	public string strGame;
	public string strDesc;
	public string strDetailTime;
	public string strDetailOther;
	public GameDesc(string strGame,string strDesc, string strDetailTime,string strDetailOther){
		this.strGame = strGame;
		this.strDesc = strDesc;
		this.strDetailTime = strDetailTime;
		this.strDetailOther = strDetailOther;
	}
}
