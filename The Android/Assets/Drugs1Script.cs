using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Drugs1Script : MonoBehaviour {

	//public SpriteRenderer[] rend;
	public Renderer[] rend;

	public GameObject[] theObjects; // item(sprites) na naka assigned sa unity
	public GameObject[] theRandomObjects; // magiging laman ng mga random items

	//public Vector2[] positions; //list of positions

	public float xMin;
	public float xMax;
	public float yMin;
	public float yMax;

	public int randomNumObj;

	public GameObject theWinPanel;
	public GameObject theHomeBtn;

	public GameObject theContinuePanel;
	public GameObject theYesBtn;
	public GameObject theNoBtn;

	public GameObject thePausePanel;
	public GameObject thePauseBtn;

	int TapCount = 0;

	int itemscanclickable = 0;
	int flag = 0;

	Text Taps;
	Text DrugsToFind;
	public void OnMouseDown(){
		
		Debug.Log ("We Pause");
		thePausePanel.SetActive (true);
		thePauseBtn.SetActive (false);
		itemscanclickable = 1;

	}
	void Start () {
		
		Taps = GameObject.Find("Taps").GetComponent<Text> ();
		DrugsToFind = GameObject.Find("DrugsToFind").GetComponent<Text> ();

		itemscanclickable = 0;
		theContinuePanel.SetActive (false);
		theWinPanel.SetActive (false);

		TapCount = 8;
		Taps.text = "Taps:" + TapCount;
		randomNumObj = 5;
		DrugsToFind.text = "Drugs:" + randomNumObj;

		theRandomObjects = new GameObject[randomNumObj]; //assign ng size sa list theRandomObjects
		rend = new Renderer[randomNumObj];
		//positions = new Vector2[randomNumObj]; //assign ng size sa list positions
		//rend = new SpriteRenderer[randomNumObj];
		int randomNum = 0 ;

		int i = 0;

		//random pick sa list theObjects and assigning to list theRandomObjects
		for(i = 0; i<randomNumObj; i++){
			randomNum = Random.Range (0,5); //pick item from theObjects[]
			theRandomObjects[i] = theObjects[randomNum]; //ibigay na kay theRandomObjects

			//rend[i] = theRandomObjects[i];
		}

		//loop through para lumabas na sa screen
		for(i = 0; i<randomNumObj; i++){	
			Instantiate(theRandomObjects[i]);
			Vector2 pos = new Vector2 (Random.Range (xMin, xMax), Random.Range (yMin, yMax)); // random coordinates
			theRandomObjects[i].transform.position = pos;
			rend[i] = theRandomObjects [i].GetComponent<Renderer>();

			rend[i].enabled = true;
		}
	
	}

	void Update(){
		flag = 0;
		Taps.text = "Taps:" + TapCount;
		DrugsToFind.text = "Drugs:" + randomNumObj;
		if(GameObject.FindWithTag("Object")==null){
			Debug.Log ("We Win to Home");
			thePauseBtn.SetActive (false);
			theWinPanel.SetActive (true);
			itemscanclickable = 1;
		}


			
		if(itemscanclickable == 0){
			
			theContinuePanel.SetActive (false);

			if(Input.GetMouseButtonDown(0)){
				Debug.Log ("Substract Update");
				TapCount--;
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				if(Physics.Raycast(ray, out hit)){

					BoxCollider bc = hit.collider as BoxCollider;
					if(bc != null){
						Destroy (bc.gameObject);
						randomNumObj--;
					}
				}

				if (TapCount <= 0) {

					Debug.Log ("OutofTaps");
					theContinuePanel.SetActive (true);
					thePauseBtn.SetActive (false);
					itemscanclickable = 1;
					//theCanvas.interactable = false;
				}

			}

		}
			

	}

	public void WinToHome(){
		Debug.Log ("We Win to Home");
		thePauseBtn.SetActive (false);

		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold += 50;
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);
		PlayerPrefs.Save();

		Application.LoadLevel ("Map Scene");

	}


	public void ContinueToYes(){
		Debug.Log ("We Continue to Yes");

		theContinuePanel.SetActive (false);
		thePauseBtn.SetActive (true);
		//theCanvas.interactable = true;
		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold -= 200;
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);
		PlayerPrefs.Save();
		Debug.Log(fltCurrGold);


		TapCount = 5;
		itemscanclickable = 0;
	}

	public void ContinueToNo(){
		Debug.Log ("We Continue to No");

		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold -= 200;		
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);		
		PlayerPrefs.Save();
		Debug.Log("I quit");

		Application.LoadLevel ("Map Scene");

	}
	public void PausetoResume(){
		Debug.Log ("We Pause to Resume");
		thePausePanel.SetActive (false);
		thePauseBtn.SetActive (true);
		itemscanclickable = 0;
		flag = 0;
	}

	public void PausetoQuit(){
		Debug.Log ("We Pause to Quit");

		float fltCurrGold = PlayerPrefs.GetFloat("PlayerGold");
		fltCurrGold -= 200;		
		PlayerPrefs.SetFloat("PlayerGold",fltCurrGold);		
		PlayerPrefs.Save();
		Debug.Log("I quit");

		Application.LoadLevel ("Map Scene");
	}
}